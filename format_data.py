# Author: David Nola
# Company: NVIDIA

from glob import glob
from skimage.io import imread
from scipy.misc import imresize
from psnr import psnr
import numpy as np
import pickle

scale_factor = 2.0

pics = [imresize(imread(f), (64,64,3))/255 for f in glob('train/dog*.jpg')]

small_pics = [imresize(x, 1/scale_factor, interp='bicubic') for x in pics]
baseline_pics = [imresize(x, scale_factor, interp='bicubic')/255 for x in small_pics]

baseline_mse = np.mean([np.mean((a-b)**2) for a,b in zip(pics[-50:],baseline_pics[-50:])])
print("Baseline MSE:", baseline_mse)

###
import matplotlib.pyplot as plt

fig = plt.figure()
fig.add_subplot(1,2,1)
plt.imshow(pics[0])
fig.add_subplot(1,2,2)
plt.imshow(small_pics[0])

pickle.dump((baseline_pics, pics), open('data.pkl', 'wb'))

plt.show()