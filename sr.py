# Author: David Nola
# Company: NVIDIA

import pickle
from keras.models import Sequential
from keras.layers import Convolution2D
import numpy as np
from psnr import psnr

baseline_pics, pics = pickle.load(open('data.pkl','rb'))


model = Sequential()

model.add(Convolution2D(64, 3, 3, subsample=(1, 1), dim_ordering='tf', activation = 'relu', border_mode='same', input_shape=(64,64,3)))
model.add(Convolution2D(64, 3, 3, subsample=(1, 1), dim_ordering='tf', activation = 'relu', border_mode='same'))
model.add(Convolution2D(64, 3, 3, subsample=(1, 1), dim_ordering='tf', activation = 'relu', border_mode='same'))
model.add(Convolution2D(64, 3, 3, subsample=(1, 1), dim_ordering='tf', activation = 'relu', border_mode='same'))
model.add(Convolution2D(64, 3, 3, subsample=(1, 1), dim_ordering='tf', activation = 'relu', border_mode='same'))

model.add(Convolution2D(3, 3, 3, subsample=(1, 1), dim_ordering='tf', activation = 'relu', border_mode='same'))

model.compile(loss='mse', optimizer='adam')

model.fit(np.array(baseline_pics[:-50]), np.array(pics[:-50]), batch_size=64, nb_epoch=20)

upsampled = list(model.predict(np.array(baseline_pics[-50:])))

new_mse = np.mean([np.mean((a-b)**2) for a,b in zip(pics[-50:],upsampled[-50:])])
print("New MSE:", new_mse)

###

import matplotlib.pyplot as plt

fig = plt.figure()
fig.add_subplot(1,3,1)
plt.imshow(pics[-50])
fig.add_subplot(1,3,2)
plt.imshow(baseline_pics[-50])
fig.add_subplot(1,3,3)
plt.imshow(np.clip(upsampled[0],0,1))

plt.show()